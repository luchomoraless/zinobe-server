

const mongoose = require('mongoose');
const environment = require('../../../src/environments/enviroments');

const mongoConnection = async () => {
    try {
        await mongoose.connect(environment.MONGO_CONNECTION.url, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useCreateIndex: true
        })
        console.log(`DB is online`);
    } catch (e) {
        console.log(`Error connect DB mongo`);
        throw new Error('Error a la hora de iniciar la BD');
    }
}

module.exports = {
    mongoConnection
}