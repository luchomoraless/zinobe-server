
const environment = require('../src/environments/enviroments');
const expressJs =  require('express');
const cors = require('cors');
const {mongoConnection} = require("../src/config/dataBase/mongoConnection");

mongoConnection();

const server = expressJs();

server.use(cors());

server.listen(environment.SERVER_PROPERTIES.port, ()=>{
    console.log(`Server is Online : [PORT]: ${environment.SERVER_PROPERTIES.port}`);
});